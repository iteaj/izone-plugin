package com.iteaj.plugin.spi;

/**
 * create time: 2020/4/4
 *
 * @author iteaj
 * @since 1.0
 */
public interface Consts {

    String LIB_DIR = "/lib";
    String BIZ_DIR = "/biz";

    String JAR_SUFFIX = ".jar";
    String FILE_PREFIX = "file:";
    String LIB_BASE_DIR = "/lib";
    String LIB_SPRING_DIR = "/spring";
    String LIB_WEB_DIR = "/web";
}
