package com.iteaj.plugin;

import com.iteaj.plugin.spi.Consts;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.iteaj.plugin.spi.Consts.FILE_PREFIX;

/**
 * create time: 2020/4/4
 *
 * @author iteaj
 * @since 1.0
 */
public class IzoneConfig {

    private static File rootDir;

    private static List<URL> WEB_LIBS = new ArrayList<>();
    private static List<URL> SPRING_LIBS = new ArrayList<>();
    private static List<URL> COMMON_LIBS = new ArrayList<>();
    static {
        File parentFile = IzoneConfig.getRootDir();
        if(parentFile != null) {
            // loader common lib
            File commonDir = new File(parentFile, Consts.LIB_BASE_DIR);
            if(commonDir.exists() && commonDir.isDirectory()) {
                File[] files = commonDir.listFiles(pathname -> pathname.getPath().endsWith(".jar"));
                if(files != null) {
                    COMMON_LIBS = Arrays.stream(files).map(libFile->{
                        try {
                            return libFile.toURI().toURL();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }).collect(Collectors.toList());
                }
            }
            // loader spring lib 类库
            File libDir = new File(parentFile, Consts.LIB_SPRING_DIR);
            if(libDir.exists() && libDir.isDirectory()) {
                File[] files = libDir.listFiles(pathname -> pathname.getPath().endsWith(".jar"));
                if(files != null) {
                    SPRING_LIBS = Arrays.stream(files).map(libFile->{
                        try {
                            return libFile.toURI().toURL();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }).collect(Collectors.toList());
                }
            }

            // loader web lib
            File webDir = new File(parentFile, Consts.LIB_WEB_DIR);
            if(webDir.exists() && webDir.isDirectory()) {
                File[] files = webDir.listFiles(pathname -> pathname.getPath().endsWith(".jar"));
                if(files != null) {
                    WEB_LIBS = Arrays.stream(files).map(libFile->{
                        try {
                            return libFile.toURI().toURL();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }).collect(Collectors.toList());
                }
            }
        }
    }

    /**
     * 返回当前项目运行的根目录
     * @return 如果目录存在返回目录， 否则返回null
     */
    public synchronized static File getRootDir() {
        if(rootDir != null) return rootDir;

        URL resource = IzoneConfig.class.getResource("/");
        String arkFile = resource.getFile().split("\\!/")[0];
        String arkFilePath = arkFile.substring(FILE_PREFIX.length());
        File file = new File(arkFilePath);
        if(file.exists()) {
            rootDir = file.getParentFile();
            if(rootDir.exists() && rootDir.isDirectory())
                return rootDir;
            else return null;
        }

        return null;
    }

    /**
     * 是否是开发环境
     * @return
     */
    public static boolean isDev() {
        return COMMON_LIBS.size() == 0;
    }

    public static List<URL> getWebLibs() {
        return WEB_LIBS;
    }

    public static void setWebLibs(List<URL> webLibs) {
        WEB_LIBS = webLibs;
    }

    public static List<URL> getSpringLibs() {
        return SPRING_LIBS;
    }

    public static void setSpringLibs(List<URL> springLibs) {
        SPRING_LIBS = springLibs;
    }

    public static List<URL> getCommonLibs() {
        return COMMON_LIBS;
    }

    public static void setCommonLibs(List<URL> commonLibs) {
        COMMON_LIBS = commonLibs;
    }
}
