package com.iteaj.plugin;

import com.alipay.sofa.ark.spi.model.PluginContext;
import com.alipay.sofa.ark.spi.registry.ServiceReference;
import com.alipay.sofa.ark.spi.service.PluginActivator;
import com.alipay.sofa.ark.spi.service.event.EventAdminService;
import com.alipay.sofa.ark.spi.service.event.EventHandler;
import com.alipay.sofa.runtime.invoke.DynamicJvmServiceProxyFinder;
import com.iteaj.plugin.event.BeforeBizStartEventHandle;
import com.iteaj.plugin.event.StartLibBizWithFinishStartup;

/**
 * create time: 2020/2/27
 *
 * @author iteaj
 * @since 1.0
 */
public class IzonePluginActivator implements PluginActivator {

    @Override
    public void start(PluginContext context) {
        ServiceReference<EventAdminService> reference = context
                .referenceService(EventAdminService.class);
        EventAdminService service = reference.getService();
        
        service.register(new BeforeBizStartEventHandle());
        service.register(new StartLibBizWithFinishStartup());

        context.publishService(DynamicJvmServiceProxyFinder.class,
                DynamicJvmServiceProxyFinder.getDynamicJvmServiceProxyFinder());
    }

    @Override
    public void stop(PluginContext context) {

    }
}
